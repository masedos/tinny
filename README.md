##Tinny urls

## Blog: https://tinny-masedos.c9users.io/

Tutorial by http://arunrocks.com/understanding-tdd-with-django

1) Initialized empty Git repository in ~/tinny/.git/

    $ git init  
    $ git config user.name "Fernandes Macedo"
    $ git config user.email masedos@egmail.com
    $ git status
    $ git add -A .
    $ git commit -m "first commit tinny urls"

2) Create a new repository on github tinny

    $ git remote add origin https://masedos@bitbucket.org/masedos/tinny.git
    $ git push -u origin master


## Starting from the Terminal

In case you want to run your Django application from the terminal just run:

3) Run migrate command to sync models to database and create Django's default superuser and auth system

    $ python manage.py makemigrations
    $ python manage.py migrate

4) Run Django

    $ python manage.py runserver $IP:$PORT
    
5) Link
    sudo ln -nsf /usr/bin/python3.4  /usr/bin/python